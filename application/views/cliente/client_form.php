
<form class="mt-5" action="<?= base_url('index.php/cliente/'.$action) ?>" method="POST">
    <label for="nome" class="grey-text">Nome do Cliente</label>
    <input type="text" name="nome" id="nome" value="<?= set_value('nome') ?>" class="form-control">

    <br>

    <div class="text-center mt-4">
        <button class="btn btn-indigo" type="submit">Cadastrar</button>
    </div>
</form>