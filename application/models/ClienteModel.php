<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/component/Table.php';

class ClienteModel extends CI_Model{

    // esses dados poderiam vir de qualquer fonte...
    private $data;

    public function __construct(){
        $this->load->library('Client');
        $this->data = $this->client->get_data();
    }

    public function table_content(){
        $labels = array('#', 'Data da Compra', 'Nome do Cliente', 'Valor da Compra');
        $table = new Table($this->data, $labels);
        $table->useHover();
        $table->useBorder();
        $table->useStripes();
        $table->useDefaultRow();
        $table->setHeaderColor('orange darken-3');
        $html = $table->getHTML();
        return $html;
    }

    public function add_client(){
        if(sizeof($_POST) == 0) return;

        // antes de usar os dados do post fazemos validação
        $this->form_validation->set_rules('nome', 'Nome do cliente', 'trim|required|min_length[5]|max_length[25]');

        if($this->form_validation->run()){
            $nome = $this->input->post('nome');
            $this->load->library('Client');
            $this->client->insert($nome);
        }
        else echo validation_errors();
    }


    public function edit_client($id_cliente){
        $this->load->library('Client');

        if(sizeof($_POST) == 0) {
            $v = $this->client->get_by_id($id_cliente);
            $_POST = $v[0];
        }
        else {
            $this->form_validation->set_rules('nome', 'Nome do cliente', 'trim|required|min_length[5]|max_length[25]');

            if($this->form_validation->run()){
                $nome = $this->input->post('nome');
                $this->client->update($nome, $id_cliente);
            }
            else echo validation_errors();
        }
    }

}