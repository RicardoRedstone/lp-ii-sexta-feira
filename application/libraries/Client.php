<?php
include_once 'util/CI_Object.php';


class Client extends CI_Object{

    public function get_data(){
        $sql = "SELECT cliente.id, data, nome, valor FROM compra, cliente WHERE cliente.id = id_cliente";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function insert($nome){
        $sql = "INSERT INTO cliente (nome) VALUES ('$nome')";
        $this->db->query($sql);
    }

    public function update($nome, $id_cliente){
        $this->db->update('cliente', 
        array('nome' => $nome), "id = $id_cliente");
    }

    public function get_by_id($id_cliente){
        $data = array('id' => $id_cliente);
        $res = $this->db->get_where('cliente', $data);
        return $res->result_array();    
    }
}