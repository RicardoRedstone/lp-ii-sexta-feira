<?php

class ImageRow{

    // definição de atributos
    private $link;
    private $title;
    private $published;
    private $image_url;
    private $description;

    public function __construct($title, $date){
        $this->title = $title;
        $this->published = $date;
    }

    public function set_description($descr){
        $this->description = $descr;
    }

    public function set_image_url($url){
        $this->image_url = $url;
    }

    /**
     * Define o endereço web do link "Saber Mais"
     * 
     * @param string: url a ser usado no link
     */
    public function set_link($url){
        $this->link = $url;
    }

    /**
     * Gera o código HTML do componente.
     * 
     * @return string: o código do componente.
     */
    public function get_html(){
        return '<div class="row wow fadeIn" data-wow-delay="0.2s">'.
                    $this->image_block().$this->text_block().    
                '</div><br>';
    }

    private function text_block(){
        return '<div class="col-lg-5">
                    <a href="#!">
                        <h2 class="post-title font-bold">'.$this->title.'</h2>
                    </a>
                    <h6 class="dark-grey-text font-bold font-small">'.$this->published.'</h6>
                    <p class="my-4 dark-grey-text">'.$this->description.'</p>
                    <div class="read-more">
                        <a href="'.$this->link.'" class="btn btn-primary btn-md">Saber Mais</a>
                    </div>
                </div>';
    }

    private function image_block(){
        return '<div class="col-lg-7">
                    <div class="view overlay hm-white-light z-depth-1 rounded">
                        <img src="'.$this->image_url.'" class="img-fluid" alt="">
                        <div class="mask"></div>
                    </div>
                </div>';
    }
}